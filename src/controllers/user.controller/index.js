const { StatusCodes } = require("http-status-codes");
const db = require("../../config/connection");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const getAll = async (req, res) => {
  try {
    db.usser
      .findAll()
      .then((data) => {
        res.status(StatusCodes.OK).send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials.",
        });
      });
  } catch (e) {
    console.log("error", e);
    res.send({ error: e, status: 404 });
  }
};

const login = async (req, res) => {
  let loginData = await db.user.findOne({
    where: { email: req.body.email },
  });

  console.log("login data....", loginData);

  loginData = JSON.parse(JSON.stringify(loginData));

  // res.status(StatusCodes.OK).send(loginData);
  if (loginData) {
    const validPass = await bcrypt.compare(
      req.body.password,
      loginData.password
    );
    if (!validPass)
      return res.status(StatusCodes.UNAUTHORIZED).send("Invalid Password.");
    loginData.token = await jwt.sign(
      { id: loginData.id, name: loginData.name, role: loginData.role },
      process.env.JWT_SECRET
    );

    // loginData['token'] = await token;

    res.status(StatusCodes.OK).send(loginData);
  } else {
    res.status(StatusCodes.UNAUTHORIZED).send({ message: "User not found" });
  }
};

const addOne = async (req, res) => {
  let password = await bcrypt.hash(req.body.password, 8);

  let row = {
    name: req.body.name,
    email: req.body.email,
    password: password,
    role: req.body.role,
  };
  try {
    console.log("hash passwor", password);

    const result = db.user
      .create(row)
      .then(function () {
        res.send(row);
      })
      .catch(function (error) {
        res.send(error);
      });
  } catch (err) {
    console.log(err);
  }
};

module.exports = { getAll, login, addOne };
