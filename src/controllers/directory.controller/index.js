const db = require("../../config/connection");
const { StatusCodes } = require("http-status-codes");
const fs = require("fs");

const getAll = async (req, res) => {
  try {
    db.directory
      .findAll({ include: db.User })
      .then((data) => {
        data = JSON.parse(JSON.stringify(data));

        let result = data.filter((e) => !e.parent_id);

        for (let index in result) {
          const children =
            data.filter((x) => x.parent_id === result[index].id) || [];

          if (children.length) {
            result[index] = { ...result[index], children };
          }
        }

        res.status(StatusCodes.OK).send(result);
      })
      .catch((err) => {
        res.status(StatusCodes.BAD_REQUEST).send({
          message:
            err.message || "Some error occurred while retrieving tutorials.",
        });
      });
  } catch (e) {
    res.send({ error: e, status: 404 });
  }
};

const addOne = async (req, res) => {
  console.log("user request data here..........", req.user);
  const name = req.body.name;
  const parent_id = req.body.parent_id || null;
  const user_id = req.body.user_id;

  let path = __dirname + "/../../../public/";
  let parentDir;

  try {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path);
    }

    if (parent_id) {
      parentDir = await db.directory
        .findOne({ where: { id: parent_id } })
        .then((data) => {
          console.log("data here......", data);
          if (!data) {
            res
              .status(StatusCodes.NOT_FOUND)
              .send({ error: "parent directory not found" });
          } else {
            return data;
          }
        });
      path += "/" + parentDir.name + "/" + name;
    } else {
      req.user.role !== "admin"
        ? res
            .status(StatusCodes.UNAUTHORIZED)
            .send({ error: "Only admin can add parent directory." })
        : (path += name);
    }

    // path = parent_id ? path + parentDir.name + "/" + name : path + name;

    if (!fs.existsSync(path)) {
      fs.mkdir(path, { recursive: true }, (err) => {
        if (err) {
          console.log("error", err);
        } else {
          const result = db.directory
            .create({
              name: name,
              parent_id: parent_id,
              user_id: user_id,
            })
            .then(function () {
              res.status(StatusCodes.CREATED).send(res.body);
            })
            .catch(function (err) {
              fs.unlink(path);
              res.status(StatusCodes.NOT_FOUND).send({ error: err });
            });

          console.log("Directory created successfully!");
        }
      });
    } else {
      res
        .status(StatusCodes.NOT_FOUND)
        .send({ error: "directory already exists" });
    }

    //   console.log("Jane's auto-generated ID:", result);
  } catch (error) {
    console.log("error here.........", error);
    res.status(StatusCodes.BAD_REQUEST).send({ error: error });
  }
};

const getOne = async (req, res) => {
  try {
    db.directory
      .findOne({ where: { id: req.params.id } })
      .then((data) => {
        res.status(StatusCodes.OK).send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials.",
        });
      });
  } catch (e) {
    console.log("error", e);
    res.send({ error: e, status: 404 });
  }
};

module.exports = { getAll, addOne, getOne };
