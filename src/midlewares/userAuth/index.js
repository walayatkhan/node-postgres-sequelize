const jwt = require("jsonwebtoken");
const userAuth = (req, res, next) => {
  const env = require("dotenv").config();
  const token = req.header("token");
  if (!token) return res.status(401).send("Access denied. Token required.");
  try {
    const decode = jwt.verify(token, process.env.JWT_SECRET);
    console.log("in autb middle ware...", decode);

    req.user = { id: decode.id, role: decode.role ? decode.role : "" };
    next();
  } catch (ex) {
    res.status(400).send("Invalid token.");
  }
};
module.exports = userAuth;
