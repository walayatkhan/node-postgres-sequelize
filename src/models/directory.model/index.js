module.exports = (sequelize, Sequelize) => {
  const Directory = sequelize.define("directory", {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notNull: { error: "name is required" },
      },
    },
    parent_id: {
      type: Sequelize.INTEGER,
    },
    user_id: Sequelize.INTEGER,
  });
  return Directory;
};
