const Router = require("express").Router();
const { directory } = require("../../../../controllers");

Router.route("/").post(directory.addOne);

module.exports = Router;
