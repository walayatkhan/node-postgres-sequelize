const Router = require("express").Router();
const { directory } = require("../../../../controllers");

Router.route("/").get(directory.getAll);
Router.route("/:id").get(directory.getOne);

module.exports = Router;
