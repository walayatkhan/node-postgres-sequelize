const Router = require("express").Router();
const userRoutes = require("./user");
const directoryRoutes = require("./directory");

const publicRoutes = [
  {
    path: "/user",
    route: userRoutes,
  },
  {
    path: "/directory",
    route: directoryRoutes,
  },
];

module.exports = { publicRoutes };
