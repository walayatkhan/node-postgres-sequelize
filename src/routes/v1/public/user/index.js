const Router = require("express").Router();
const { user } = require("../../../../controllers");

Router.route("/").get(user.getAll).post(user.addOne);

Router.route("/login").post(user.login);

module.exports = Router;
