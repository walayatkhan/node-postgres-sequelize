const Router = require("express").Router();
const { publicRoutes } = require("./v1/public");
const directoryRoutes = require("../routes/v1/public/directory/auth");

const { authorize, userAuth } = require("../midlewares");

let version = `/${process.env.VER}`;

publicRoutes.forEach((route) => {
  Router.use(version + route.path, authorize, route.route);
});

Router.use(version + "/directory", [authorize, userAuth], directoryRoutes);
module.exports = Router;
