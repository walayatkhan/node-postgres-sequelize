const express = require("express");
const app = express();
const env = require("dotenv").config();
const routes = require("./src/routes");
const initializer = require("./src/helpers");
const db = require("./src/config/connection");

// const cors = require("cors");
// var corsOptions = {
//   origin: "http://localhost:8081",
// };

// app.use(cors(corsOptions));

// initializer();
// parse requests of json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.use(routes);

db.sequelize.sync();

app.listen(process.env.PORT || 4444, () => {
  console.log(`Server is running on port ${process.env.PORT || 4444}`);
});

module.exports = app;
